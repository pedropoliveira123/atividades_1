/*Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o seu salário a receber, sabendo-
se que esse funcionário tem gratificação de R$50,00 e paga
imposto de 10% sobre o salário-base.
*/

namespace exercicio_7
{
    //Entrada de dados
    let salariobase, salarioareceber: number;

    salariobase = 1200;

    //Processo de dados
    salarioareceber = salariobase + 50 - (salariobase * 10/100);

    //Saída
    console.log(`O salário a receber é: ${salarioareceber}`);


}