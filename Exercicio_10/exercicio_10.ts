/*Faça um programa que calcule e mostre a área de um
círculo. Sabe-se que: Área = π.R2
*/

namespace exercicio_10
{
    //Entrada
    let π, raio: number;

    π = 3;
    raio = 5;
    let area: number;

    //Processo
    area = π * (raio * raio);

    //Saída
    console.log(`A area do circulo é: ${area}`);
}