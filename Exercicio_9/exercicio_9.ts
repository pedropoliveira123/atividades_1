/*Faça um programa que calcule e mostre a área de um
triângulo. Sabe-se que: Área = (base * altura)/2.
*/

namespace exercicio_9
{
    //Entrada
    let altura, base: number;

    altura = 5;
    base = 3;
    let area: number;

    //Processo
    area = (base * altura) /2;

    //Saída
    console.log(`A area do triangulo é: ${area}`);
}