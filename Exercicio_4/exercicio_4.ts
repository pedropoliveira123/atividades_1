/*Faça um programa que receba o salário de um funcionário,
calcule e mostre o novo salário, sabendo-se que este sofreu
um aumento de 25%.
*/

namespace exercicio_4
{
    //Entrada de dados
    let salario: number;

    salario = 2700;
    let novosalario: number;

    //Processo de dados
    novosalario = salario * 1.25;
    //Ou pode se escrever
    novosalario = salario + (salario * 25/100);

    //Saída
    console.log(`O calculo do salario é: ${novosalario}`);
}