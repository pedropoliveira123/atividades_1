/*Faça um programa que receba o salário de um funcionário e
o percentual de aumento, calcule e mostre o valor do
aumento e o novo salário.
*/

namespace exercicio_5
{
    //Entrada de dados
    let salarioantigo, aumento: number;

    salarioantigo = 1500;
    aumento = 10;
    let valoraumento: number;
    let salarionovo: number;

    //Processamento de dados
    valoraumento = salarioantigo * aumento/100;
    salarionovo = salarioantigo + valoraumento;

    //Saída
    console.log(`O salario novo é: ${salarionovo}`);
}