    /*Faça um programa que receba três notas, calcule e mostre a
média aritmética entre elas.
*/
namespace exercicio_2
{
    let nota1, nota2, nota3: number;

    nota1 = 6;
    nota2 = 8;
    nota3 = 5;

    let media: number;

    media = (nota1 + nota2 + nota3) /3;
    
    console.log(`A media final é ${media}`);
}