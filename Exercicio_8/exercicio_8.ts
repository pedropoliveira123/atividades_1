/*Faça um programa que receba o valor de um depósito e o
valor da taxa de juros, calcule e mostre o valor do rendimento
e o valor total depois do rendimento.
*/

namespace exercicio_8
{
    //Entrada
    let deposito, taxadejuros: number;

    deposito = 500;
    taxadejuros = 20;
    let rendimento, valortotal: number;

    //Processo
    rendimento = deposito + (deposito * taxadejuros/100);
    valortotal = deposito + rendimento;

    //Saída
    console.log(`O valor total a ser recebido: ${valortotal}`);
}