/*Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o salário a receber, sabendo-se
que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base.
*/

namespace exercicio_6
{
    //Entrada de dados
    let salariobase: number;

    salariobase = 2500;
    let salarioareceber: number;

    //Processo de dados
    salarioareceber = salariobase + (salariobase * 5/100) - (salariobase * 7/100);
    //Ou pode se escrever
    salarioareceber = salariobase - (salariobase * 2/100);

    //Saída
    console.log(`O salario a receber: ${salarioareceber}`);
}