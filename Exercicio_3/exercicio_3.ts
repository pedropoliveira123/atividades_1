/*Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas.
*/

namespace exercicio_3
{
    //Entrada de dados
    let nota1, nota2, nota3, peso1, peso2, peso3: number;

    nota1 = 5;
    nota2 = 8;
    nota3 = 10;
    peso1 = 3;
    peso2 = 2;
    peso3 = 1;
    
    let media: number;


    //Processamento
    media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) /(peso1 + peso2 + peso3);

    //Saída
    console.log(`A media ponderada é ${media}`);
}